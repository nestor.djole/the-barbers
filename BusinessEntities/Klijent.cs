﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public class Klijent : AbstractBusinessEntity
    {     
        public int KlijentId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string BrojTelefona { get; set; }
        public string EmailAdresa { get; set; }

        public override string ToString()
        {
            return $"{Prezime} {Ime}";
        }

        public override List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader)
        {
            List<AbstractBusinessEntity> objects = new List<AbstractBusinessEntity>();
            while (reader.Read())
            {
                Klijent c = new Klijent()
                {
                    KlijentId = (int)reader["KlijentId"],
                    Ime = reader["Ime"].ToString(),
                    Prezime = reader["Prezime"].ToString(),
                    BrojTelefona = reader["BrojTelefona"].ToString(),
                    EmailAdresa = reader["EmailAdresa"].ToString()
                };
                objects.Add(c);
            }
            return objects;
        }

        public override string GetColumnNames()
        {
            return "KlijentId, Ime, Prezime, BrojTelefona, EmailAdresa";
        }

        public override string GetColumnValues()
        {
            return $"'{KlijentId}','{Ime}', '{Prezime}', '{BrojTelefona}', '{EmailAdresa}'";
        }

        public override string GetDefaultSearchCondition()
        {
            return $"KlijentId={KlijentId}";
        }

        public override string GetIDName()
        {
            return "KlijentId";
        }

        public override string GetTableName()
        {
            return "Klijent";
        }

        public override string GetUpdateColumnValuesQuery()
        {
            return $"Ime='{Ime}', Prezime='{Prezime}', BrojTelefona='{BrojTelefona}', EmailAdresa='{EmailAdresa}'";
        }
    }
}
