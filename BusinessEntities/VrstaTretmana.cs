﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public class VrstaTretmana : AbstractBusinessEntity
    {
        public int VrstaTretmanaId { get; set; }
        public string NazivVrsteTretmana { get; set; }

        public override List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader)
        {
            List<AbstractBusinessEntity> objects = new List<AbstractBusinessEntity>();
            while (reader.Read())
            {
                VrstaTretmana tt = new VrstaTretmana()
                {
                    VrstaTretmanaId = (int)reader["VrstaTretmanaId"],
                    NazivVrsteTretmana = reader["NazivVrsteTretmana"].ToString()
                };
                objects.Add(tt);
            }
            return objects;
        }

        public override string ToString()
        {
            return NazivVrsteTretmana;
        }

        public override string GetColumnNames()
        {
            return "VrstaTretmanaId, NazivVrsteTretmana";
        }

        public override string GetColumnValues()
        {
            return $"'{VrstaTretmanaId}','{NazivVrsteTretmana}'";
        }

        public override string GetDefaultSearchCondition()
        {
            return $"VrstaTretmanaId={VrstaTretmanaId}";
        }

        public override string GetIDName()
        {
            return "VrstaTretmanaId";
        }

        public override string GetTableName()
        {
            return "VrstaTretmana";
        }

        public override string GetUpdateColumnValuesQuery()
        {
            return $"NazivVrsteTretmana='{NazivVrsteTretmana}'";
        }
    }
}
