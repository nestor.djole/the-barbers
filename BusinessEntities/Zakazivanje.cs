﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public class Zakazivanje : AbstractBusinessEntity
    {
        public DateTime Datum { get; set; }
        public TimeSpan Vreme { get; set; }
        public string Opis { get; set; }
        public Klijent Klijent { get; set; }
        public Zaposleni Zaposleni { get; set; }
        public Tretman Tretman { get; set; }


        public override bool Equals(object obj)
        {
            if(obj is Zakazivanje _event)
            {
                return Datum.Equals(_event.Datum) 
                    && Vreme.Equals(_event.Vreme) 
                    && Tretman.TretmanId.Equals(_event.Tretman.TretmanId) 
                    && Zaposleni.ZaposleniId.Equals(_event.Zaposleni.ZaposleniId);
            }
            return false;
        }

        public override List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader)
        {
            List<AbstractBusinessEntity> objects = new List<AbstractBusinessEntity>();
            while (reader.Read())
            {
                Zakazivanje e = new Zakazivanje()
                {
                    Datum = (DateTime)reader["Datum"],
                    Vreme = (TimeSpan)reader["Vreme"],
                    Opis = reader["Opis"].ToString(),
                    Klijent = new Klijent
                    {
                        KlijentId = (int)reader["KlijentId"]
                    },
                    Zaposleni = new Zaposleni
                    {
                        ZaposleniId = (int)reader["ZaposleniId"]
                    },
                    Tretman = new Tretman
                    {
                        TretmanId = (int)reader["TretmanId"]
                    },
                };
                objects.Add(e);
            }
            return objects;
        }

        public override string GetColumnNames()
        {
            return "Datum, Vreme, Opis, ZaposleniId, TretmanId, KlijentId";
        }

        public override string GetColumnValues()
        {
            return $"'{Datum}','{Vreme}', '{Opis}', '{Zaposleni.ZaposleniId}', '{Tretman.TretmanId}', '{Klijent.KlijentId}'";
        }

        public override string GetDefaultSearchCondition()
        {
            return $"Datum='{Datum}' AND Vreme='{Vreme}' AND ZaposleniId={Zaposleni.ZaposleniId} AND TretmanId={Tretman.TretmanId}";
        }

        public override string GetIDName()
        {
            throw new NotImplementedException();
        }

        public override string GetTableName()
        {
            return "Zakazivanje";
        }

        public override string GetUpdateColumnValuesQuery()
        {
            return $"Datum='{Datum}', Vreme='{Vreme}', Opis='{Opis}', ZaposleniId={Zaposleni.ZaposleniId}, TretmanId='{Tretman.TretmanId}', KlijentId='{Klijent.KlijentId}'";
        }
    }
}
