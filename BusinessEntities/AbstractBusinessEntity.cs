﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public abstract class AbstractBusinessEntity
    {
        private string searchCondition = "";
        //to do za sort kriterijum

        public void setSearchCondition(string searchCondition)
        {
            this.searchCondition = $" WHERE {searchCondition}";
        }

        public abstract string GetIDName();
        public abstract string GetTableName();
        public abstract string GetColumnNames();
        public abstract string GetColumnValues();
        public abstract string GetUpdateColumnValuesQuery();
        public abstract string GetDefaultSearchCondition();
        public abstract List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader);

        public string SearchCondition
        {
            get
            {
                return searchCondition;
                //TODO getDefault
            }
        }
    }
}
