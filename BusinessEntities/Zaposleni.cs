﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public class Zaposleni : AbstractBusinessEntity
    {
        public int ZaposleniId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string BrojTelefona { get; set; }

        public override string ToString()
        {
            return $"{Prezime} {Ime}";
        }

        public override List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader)
        {
            List<AbstractBusinessEntity> objects = new List<AbstractBusinessEntity>();
            while (reader.Read())
            {
                Zaposleni c = new Zaposleni()
                {
                    ZaposleniId = (int)reader["ZaposleniId"],
                    Ime = reader["Ime"].ToString(),
                    Prezime = reader["Prezime"].ToString(),
                    BrojTelefona = reader["BrojTelefona"].ToString()
                };
                objects.Add(c);
            }
            return objects;
        }

        public override string GetColumnNames()
        {
            return "ZaposleniId, Ime, Prezime, BrojTelefona";
        }

        public override string GetColumnValues()
        {
            return $"'{ZaposleniId}','{Ime}', '{Prezime}', '{BrojTelefona}'";
        }

        public override string GetDefaultSearchCondition()
        {
            return $"ZaposleniId={ZaposleniId}";
        }

        public override string GetIDName()
        {
            return "ZaposleniId";
        }

        public override string GetTableName()
        {
            return "Zaposleni";
        }

        public override string GetUpdateColumnValuesQuery()
        {
            return $"Ime='{Ime}', Prezime='{Prezime}', BrojTelefona='{BrojTelefona}'";
        }
    }
}
