﻿using System;

namespace BusinessEntities.TransferModels
{
    [Serializable]
    public class Request
    {
        public Operation Operation { get; set; }
        public object RequestObject { get; set; }
    }

    public enum Operation
    {
        GET_ALL_CLIENTS,
        INSERT_CLIENT,
        UPDATE_CLIENT,
        DELETE_CLIENT,
        GET_ALL_TREATMENTS,
        INSERT_TREATMENT,
        UPDATE_TREATMENT,
        DELETE_TREATMENT,
        GET_ALL_TREATMENTTYPES,
        GET_ALL_EVENTS,
        INSERT_EVENT,
        UPDATE_EVENT,
        DELETE_EVENT,
        GET_ALL_EMPLOYEES,
        LOGIN,
        END
    }
}
