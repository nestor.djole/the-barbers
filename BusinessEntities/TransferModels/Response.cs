﻿using System;

namespace BusinessEntities.TransferModels
{
    [Serializable]
    public class Response
    {
        public bool Successful { get; set; }
        public object Result { get; set; }
    }
}
