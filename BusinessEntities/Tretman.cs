﻿ using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace BusinessEntities
{
    [Serializable]
    public class Tretman : AbstractBusinessEntity
    {
        public int TretmanId { get; set; }
        public string NazivTretmana { get; set; }
        public int Trajanje { get; set; }
        public VrstaTretmana Vrsta { get; set; }

        public override string ToString()
        {
            return $"{NazivTretmana} ({Trajanje} min)";
        }

        public override List<AbstractBusinessEntity> CreateBusinessEntity(SqlDataReader reader)
        {
            List<AbstractBusinessEntity> objects = new List<AbstractBusinessEntity>();
            while (reader.Read())
            {
                Tretman t = new Tretman()
                {
                    TretmanId = (int)reader["TretmanId"],
                    NazivTretmana = reader["NazivTretmana"].ToString(),
                    Trajanje = (int)reader["Trajanje"],
                    Vrsta = new VrstaTretmana
                    {
                        VrstaTretmanaId = (int)reader["VrstaTretmanaId"]
                    }
                };
                objects.Add(t);
            }
            return objects;
        }

        public override string GetColumnNames()
        {
            return "TretmanId, NazivTretmana, Trajanje, VrstaTretmanaId";
        }

        public override string GetColumnValues()
        {
            return $"'{TretmanId}', '{NazivTretmana}', '{Trajanje}', {Vrsta.VrstaTretmanaId}";
        }

        public override string GetDefaultSearchCondition()
        {
            return $"TretmanId={TretmanId}";
        }

        public override string GetIDName()
        {
            return "TretmanId";
        }

        public override string GetTableName()
        {
            return "Tretman";
        }

        public override string GetUpdateColumnValuesQuery()
        {
            return $"NazivTretmana='{NazivTretmana}', Trajanje='{Trajanje}', VrstaTretmanaId={Vrsta.VrstaTretmanaId}";
        }
    }
}
