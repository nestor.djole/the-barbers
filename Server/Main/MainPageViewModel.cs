﻿using ServerApp.Base.ViewModel;
using ServerApp.Server;
using System;

namespace ServerApp.Main
{
    public class MainWindowViewModel : ContainerViewModelBase, IMainPageViewModel
    {
        private IServerViewModel _serverViewModel;


        public MainWindowViewModel(
            IServerViewModel serverViewModel
            )
        {
            _serverViewModel = serverViewModel;
            _serverViewModel.Started += serverViewModel_Started;

            setServerPageCurrent();
        }

        private void setServerPageCurrent()
        {
            _serverViewModel.Start();
        }


        private void serverViewModel_Started(object sender, EventArgs e)
        {
            CurrentViewModel = _serverViewModel;
        }
      
    }
}