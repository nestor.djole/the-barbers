﻿using Server.RequestHandler;
using ServerApp.Base.ViewModel;
using ServerApp.Common;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows;

namespace ServerApp.Server
{
    public enum ServerViewModelResultType
    {
        RequestStartServer,
        RequestStopServer
    }

    public class ServerViewModel : ViewModelBase, IServerViewModel
    {
        private static Socket serverSocket;
        private List<RequestHandler> connectedUsers = new List<RequestHandler>();

        private bool _stopButtonVisible;
        private bool _startButtonVisible;

        private RelayCommand _startServerCommand;
        private RelayCommand _stopServerCommand;

        public event EventHandler Started;

        public ServerViewModel()
        {
            _startServerCommand = new RelayCommand(executeStartServerCommand);
            _stopServerCommand = new RelayCommand(executeStopServerCommand);
        }

        public void Start()
        {
            StopButtonVisible = false;
            StartButtonVisible = true;
            Started?.Invoke(this, new EventArgs());
        }

        public RelayCommand StartServerCommand
        {
            get
            {
                return _startServerCommand;
            }
        }

        public RelayCommand StopServerCommand
        {
            get
            {
                return _stopServerCommand;
            }
        }

        public bool StopButtonVisible
        {
            get
            {
                return _stopButtonVisible;
            }
            set
            {
                if (_stopButtonVisible != value)
                {
                    _stopButtonVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool StartButtonVisible
        {
            get
            {
                return _startButtonVisible;
            }
            set
            {
                if (_startButtonVisible != value)
                {
                    _startButtonVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        private void executeStartServerCommand()
        {
            Thread serverThread = new Thread(startServer);
            serverThread.Start();
            serverThread.IsBackground = true;
            StopButtonVisible = true;
        }

        private void executeStopServerCommand()
        {
            try
            {
                stopServer();            
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Грешка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void startServer()
        {
            RequestHandler connecteduser = null;
            try
            {
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(new IPEndPoint(IPAddress.Any, 10000));
                serverSocket.Listen(5);

                bool end = false;

                while (!end)
                {
                    Socket user = serverSocket.Accept();
                    connecteduser = new RequestHandler(user);
                    connectedUsers.Add(connecteduser);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Грешка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {                
                serverSocket?.Close();
            }
        }    

        private void stopServer()
        {
            if (connectedUsers.Count > 0)
            {
                throw new Exception($"Сервер не може да се заустави. Број повезаних клијената: {connectedUsers.Count}!");
            }

            serverSocket?.Close();

            foreach (RequestHandler cm in connectedUsers)
            {
                if(cm.Socket.Connected)
                {
                    cm.Socket.Shutdown(SocketShutdown.Both);
                    cm.Socket.Close();
                }
            }

            StopButtonVisible = false;
            StartButtonVisible = true;
        }
    }
}