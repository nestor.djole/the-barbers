﻿using ServerApp.Base.ViewModel;
using ServerApp.Common;
using System;

namespace ServerApp.Server
{
    public interface IServerViewModel : IViewModel
    {
        RelayCommand StartServerCommand { get; }
        RelayCommand StopServerCommand { get; }

        event EventHandler Started;

        void Start();
    }
}
