﻿using ServerApp.Common;
using ServerApp.Main;
using System.Windows;

namespace ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = ServiceProvider.Instance.GetService(typeof(IMainPageViewModel));
        }
    }
}
