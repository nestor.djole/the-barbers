﻿using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using BusinessEntities.TransferModels;

namespace Server.RequestHandler
{
    public class RequestHandler
    {
        public Socket Socket { get; }
        private NetworkStream _stream;
        private BinaryFormatter formatter = new BinaryFormatter();

        public RequestHandler(Socket userSocket)
        {
            Socket = userSocket;
            _stream = new NetworkStream(Socket);
            new Thread(HandleRequest) { IsBackground = true }.Start();
        }

        public void HandleRequest()
        {
            try
            {
                bool end = false;
                while (!end)
                {
                    Request request = (Request)formatter.Deserialize(_stream);
                    Response response = new Response();
                    try
                    {
                        BusinessOperations.BaseBusinessOperation baseOperation = BusinessOperationsController.BusinessOperationsController.Instance.GetOperation(request.Operation);
                        object operationResult = baseOperation.ExecuteOperation(request.RequestObject);
                        response.Result = operationResult;
                        response.Successful = true;
                    }
                    catch (Exception e)
                    {
                        response.Successful = false;
                        response.Result = e.Message;
                    }

                    formatter.Serialize(_stream, response);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (Socket.Connected)
                    Socket.Shutdown(SocketShutdown.Both);

                Socket.Close();
            }
        }
    }
}
