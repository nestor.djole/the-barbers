﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace DataAccessComponents
{
    public class DatabaseBroker
    {
        private SqlConnection _connection;
        private SqlTransaction _transaction;
        private static DatabaseBroker _broker;

        private DatabaseBroker()
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
        }

        public static DatabaseBroker Broker
        {
            get
            {
                if (_broker == null)
                    _broker = new DatabaseBroker();

                return _broker;
            }
        }

        public void OpenConnection()
        {
            _connection.Open();
        }

        public void CloseConnection()
        {
            _connection.Close();
        }

        public void BeginTransaction()
        {
            _transaction = _connection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }

        public object Create(AbstractBusinessEntity abstractModel)
        {
            SqlCommand command = null;
            try
            {
                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"INSERT INTO {abstractModel.GetTableName()}({abstractModel.GetColumnNames()}) VALUES({abstractModel.GetColumnValues()})";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return abstractModel;
                else
                    throw new Exception($"Унос података није успео");
            }
            catch (Exception e)
            {
                throw new Exception($"Грешка: {e.Message}!");
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
            }
        }

        public object Update(AbstractBusinessEntity abstractModel)
        {
            SqlCommand command = null;
            try
            {
                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"UPDATE {abstractModel.GetTableName()} SET {abstractModel.GetUpdateColumnValuesQuery()} WHERE {abstractModel.GetDefaultSearchCondition()}";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return abstractModel;
                else
                    throw new Exception($"Ажурирање није успело");
            }
            catch (Exception e)
            {
                throw new Exception($"Грешка: {e.Message}!");
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
            }
        }

        public object Delete(AbstractBusinessEntity abstractModel)
        {
            SqlCommand command = null;
            try
            {
                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"DELETE FROM {abstractModel.GetTableName()} WHERE {abstractModel.GetDefaultSearchCondition()}";
                command.CommandText = query;
                if (command.ExecuteNonQuery() > 0)
                    return abstractModel;
                else
                    throw new Exception($"Брисање није успело");

            }
            catch (Exception e)
            {
                throw new Exception($"Грешка: {e.Message}!");
            }
            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }

        public List<AbstractBusinessEntity> Select(AbstractBusinessEntity abstractModel)
        {
            SqlDataReader reader = null;
            SqlCommand command = null;
            try
            {
                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"SELECT * FROM {abstractModel.GetTableName()}{abstractModel.SearchCondition}";
                command.CommandText = query;
                reader = command.ExecuteReader();
                return abstractModel.CreateBusinessEntity(reader);
            }
            catch (Exception e)
            {
                throw new Exception($"Грешка: {e.Message}!");
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                    reader.Close();
            }
        }

        public int GetNewID(AbstractBusinessEntity abstractModel)
        {
            SqlCommand command = null;
            try
            {

                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"SELECT MAX({abstractModel.GetIDName()}) AS m FROM {abstractModel.GetTableName()}";
                command.CommandText = query;
                command.CommandType = System.Data.CommandType.Text;

                var result = command.ExecuteScalar();

                if (result == DBNull.Value)
                {
                    return 1;
                }
                return (int)result + 1;
            }

            finally
            {
                if (command != null)
                    command.Dispose();
            }
        }

        public AbstractBusinessEntity GetById(AbstractBusinessEntity abstractModel)
        {
            SqlDataReader reader = null;
            SqlCommand command = null;
            try
            {
                command = _connection.CreateCommand();
                command.Transaction = _transaction;
                string query = $"SELECT * FROM {abstractModel.GetTableName()} WHERE {abstractModel.GetDefaultSearchCondition()}";
                command.CommandText = query;
                reader = command.ExecuteReader();
                return abstractModel.CreateBusinessEntity(reader).First();
            }
            catch (Exception e)
            {
                throw new Exception($"Грешка: {e.Message}!");
            }
            finally
            {
                if (command != null)
                    command.Dispose();

                if (reader != null)
                    reader.Close();
            }
        }
    }
}
