﻿using BusinessEntities.TransferModels;
using BusinessOperations;
using BusinessOperations.Clients;

namespace BusinessOperationController
{
    public class BusinessOperationController
    {
        private static BusinessOperationController _instance;

        private BusinessOperationController()
        {

        }

        public static BusinessOperationController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BusinessOperationController();

                return _instance;
            }
        }

        public BaseBusinessOperation GetOperation(Operation requestOperation)
        {
            switch (requestOperation)
            {
                case Operation.GET_ALL_CLIENTS:
                    return new GetAllClientsOperation();
                case Operation.INSERT_CLIENT:
                    return new InsertClientOperation();
                case Operation.UPDATE_CLIENT:
                    return new UpdateClientOperation();
                case Operation.DELETE_CLIENT:
                    return new DeleteClientOperation();
                //case Operation.GET_ALL_TREATMENTS:
                //    return new ObrisiKlijentaSO();
                //case Operation.INSERT_TREATMENT:
                //    return new IzmeniKlijentaSO();
                //case Operation.UPDATE_TREATMENT:
                //    return new VratiSveVrsteTretmanaSO();
                //case Operation.DELETE_TREATMENT:
                //    return new VratiSveTretmaneSO();
                //case Operation.LOGIN:
                //    return new SacuvajTretmanSO();
                //case Operation.END:
                //    return new ObrisiTretmanSO();

                default: return null;
            }
        }
    }
}
