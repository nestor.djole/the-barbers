﻿using BusinessEntities.TransferModels;
using BusinessOperations;
using BusinessOperations.Clients;
using BusinessOperations.Treatments;
using BusinessOperations.Scheduling;
using BusinessOperations.Employees;

namespace BusinessOperationsController
{
    public class BusinessOperationsController
    {
        private static BusinessOperationsController _instance;

        private BusinessOperationsController()
        {

        }

        public static BusinessOperationsController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new BusinessOperationsController();

                return _instance;
            }
        }

        public BaseBusinessOperation GetOperation(Operation requestOperation)
        {
            switch (requestOperation)
            {
                case Operation.GET_ALL_CLIENTS:
                    return new GetAllClientsOperation();
                case Operation.INSERT_CLIENT:
                    return new InsertClientOperation();
                case Operation.UPDATE_CLIENT:
                    return new UpdateClientOperation();
                case Operation.DELETE_CLIENT:
                    return new DeleteClientOperation();
                case Operation.GET_ALL_TREATMENTS:
                    return new GetAllTreatmentsOperation();
                case Operation.INSERT_TREATMENT:
                    return new InsertTreatmentOperation();
                case Operation.UPDATE_TREATMENT:
                    return new UpdateTreatmentOperation();
                case Operation.DELETE_TREATMENT:
                    return new DeleteTreatmentOperation();
                case Operation.GET_ALL_TREATMENTTYPES:
                    return new GetAllTreatmentTypesOperation();
                case Operation.GET_ALL_EVENTS:
                    return new GetAllEventsOperation();
                case Operation.INSERT_EVENT:
                    return new InsertEventOperation();
                case Operation.UPDATE_EVENT:
                    return new UpdateEventOperation();
                case Operation.DELETE_EVENT:
                    return new DeleteEventOperation();
                case Operation.GET_ALL_EMPLOYEES:
                    return new GetAllEmployeesOperation();

                default: return null;
            }
        }
    }
}