﻿using System.Windows;
using TheBarbers.Common;
using TheBarbers.Main;

namespace TheBarbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = ServiceProvider.Instance.GetService(typeof(IMainPageViewModel));
        }
    }
}
