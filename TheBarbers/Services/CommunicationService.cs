﻿using BusinessEntities.TransferModels;
using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace TheBarbers.Services
{
    public class CommunicationService
    {
        private static CommunicationService _instance;
        private TcpClient _client;
        private NetworkStream _stream;
        private BinaryFormatter _formatter;

        private CommunicationService()
        {
            _client = new TcpClient("127.0.0.1", 10000);
            _stream = _client.GetStream();
            _formatter = new BinaryFormatter();
        }

        public static CommunicationService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new CommunicationService();

                return _instance;
            }
        }

        public BinaryFormatter Formatter
        {
            get
            {
                return _formatter;
            }
        }

        public NetworkStream Stream
        {
            get
            {
                return _stream;
            }
        }

        public bool IsConnectionOpen()
        {
            return _client.Connected;
        }

        public object SendRequest(Operation operation, object businessObject)
        {
            Request request = new Request()
            {
                Operation = operation,
                RequestObject = businessObject
            };

            Formatter.Serialize(Stream, request);
            Response response = (Response)Formatter.Deserialize(Stream);

            if (response.Successful)
                return response.Result;
            else
                throw new Exception(response.Result.ToString());
        }
    }
}
