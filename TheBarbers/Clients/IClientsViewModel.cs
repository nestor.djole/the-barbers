﻿using BusinessEntities;
using System;
using System.Collections.ObjectModel;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Clients
{
    public interface IClientsViewModel : IContainerViewModel
    {
        RelayCommand GoBackCommand { get; }
        RelayCommand CreateClientCommand { get; }
        RelayCommand UpdateClientCommand { get; }
        RelayCommand DeleteClientCommand { get; }
        RelayCommand SearchClientsCommand { get; }

        event EventHandler Started;
        event EventHandler LoadData;
        event EventHandler<ClientEventArgs> CreateClient;
        event EventHandler<ClientEventArgs> UpdateClient;
        event EventHandler<ClientEventArgs> DeleteClient;
        event EventHandler Succeeded;

        ObservableCollection<Klijent> AllClients { get; set; }
        ObservableCollection<Klijent> ClientsToShow { get; set; }

        void Start();
        void LoadClientsData();
    }
}
