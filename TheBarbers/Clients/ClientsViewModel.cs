﻿using BusinessEntities;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Clients
{
    public class ClientsViewModel : ContainerViewModelBase, IClientsViewModel
    {
        private RelayCommand _goBackCommand;
        private RelayCommand _createClientCommand;
        private RelayCommand _startUpdateClientCommand;
        private RelayCommand _updateClientCommand;
        private RelayCommand _cancelUpdateClientCommand;
        private RelayCommand _deleteClientCommand;
        private RelayCommand _searchClientsCommand;

        public ObservableCollection<Klijent> _clientsToShow;

        private string _firstName;
        private string _lastName;
        private string _phoneNumber;
        private string _email;
        private string _searchParametar;

        private bool _createMode;
        private bool _updateMode;

        public ClientsViewModel()
        {
            _goBackCommand = new RelayCommand(executeGoBackCommand);
            _createClientCommand = new RelayCommand(executeCreateClientCommand);
            _startUpdateClientCommand = new RelayCommand(executeStartUpdateClientCommand);
            _updateClientCommand = new RelayCommand(executeUpdateClientCommand);
            _cancelUpdateClientCommand = new RelayCommand(executeCancelUpdateClientCommand);
            _deleteClientCommand = new RelayCommand(executeDeleteClientCommand);
            _searchClientsCommand = new RelayCommand(executeSearchClientsCommand);
            AllClients = new ObservableCollection<Klijent>();
            ClientsToShow = new ObservableCollection<Klijent>();
            SetDefaultFieldsAndSelections();
        }

        public event EventHandler Started;
        public event EventHandler LoadData;
        public event EventHandler Succeeded;
        public event EventHandler<ClientEventArgs> CreateClient;
        public event EventHandler<ClientEventArgs> UpdateClient;
        public event EventHandler<ClientEventArgs> DeleteClient;

        public RelayCommand GoBackCommand
        {
            get
            {
                return _goBackCommand;
            }
        }
        public RelayCommand CreateClientCommand
        {
            get
            {
                return _createClientCommand;
            }
        }
        public RelayCommand StartUpdateClientCommand
        {
            get
            {
                return _startUpdateClientCommand;
            }
        }
        public RelayCommand UpdateClientCommand
        {
            get
            {
                return _updateClientCommand;
            }
        }
        public RelayCommand CancelUpdateClientCommand
        {
            get
            {
                return _cancelUpdateClientCommand;
            }
        }
        public RelayCommand DeleteClientCommand
        {
            get
            {
                return _deleteClientCommand;
            }
        }
        public RelayCommand SearchClientsCommand
        {
            get
            {
                return _searchClientsCommand;
            }
        }

        public ObservableCollection<Klijent> AllClients { get; set; }
        public ObservableCollection<Klijent> ClientsToShow
        {
            get
            {
                return _clientsToShow;
            }
            set
            {
                if (_clientsToShow != value)
                {
                    _clientsToShow = value;
                    OnPropertyChanged(nameof(this.ClientsToShow));
                }
            }
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                if (_firstName != value)
                {
                    _firstName = value;
                    OnPropertyChanged(nameof(this.FirstName));
                }
            }
        }
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                if (_lastName != value)
                {
                    _lastName = value;
                    OnPropertyChanged(nameof(this.LastName));
                }
            }
        }
        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                if (_phoneNumber != value)
                {
                    _phoneNumber = value;
                    OnPropertyChanged(nameof(this.PhoneNumber));
                }
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (_email != value)
                {
                    _email = value;
                    OnPropertyChanged(nameof(this.Email));
                }
            }
        }
        public string SearchParametar
        {
            get
            {
                return _searchParametar;
            }
            set
            {
                if (_searchParametar != value)
                {
                    _searchParametar = value;
                    OnPropertyChanged(nameof(this.SearchParametar));
                }
            }
        }

        public Klijent SelectedClient { get; set; }

        public bool CreateMode
        {
            get
            {
                return _createMode;
            }
            set
            {
                if (_createMode != value)
                {
                    _createMode = value;
                    OnPropertyChanged(nameof(this.CreateMode));
                }
            }
        }
        public bool UpdateMode
        {
            get
            {
                return _updateMode;
            }
            set
            {
                if (_updateMode != value)
                {
                    _updateMode = value;
                    OnPropertyChanged(nameof(this.UpdateMode));
                }
            }
        }

        public void Start()
        {
            Started?.Invoke(this, new EventArgs());
            SetDefaultFieldsAndSelections();
        }

        public void LoadClientsData()
        {
            LoadData?.Invoke(this, new EventArgs());
            ShowAllClients();
        }

        private void executeGoBackCommand()
        {
            Succeeded?.Invoke(this, new EventArgs());
        }

        private void executeCreateClientCommand()
        {
            StringBuilder errorMessage;

            if (!isInputValidated(out errorMessage))
            {
                MessageBox.Show(errorMessage.ToString(), "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            CreateClient?.Invoke(this, new ClientEventArgs(new Klijent() { Ime = FirstName, Prezime = LastName, BrojTelefona = PhoneNumber, EmailAdresa = Email }));
            SetDefaultFieldsAndSelections();
            ShowAllClients();
        }

        private void executeStartUpdateClientCommand()
        {
            if (SelectedClient == null)
            {
                MessageBox.Show("Морате изабрати клијента!", "Грешка!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            CreateMode = false;
            UpdateMode = true;

            FirstName = SelectedClient.Ime;
            LastName = SelectedClient.Prezime;
            PhoneNumber = SelectedClient.BrojTelefona;
            Email = SelectedClient.EmailAdresa;
        }

        private void executeUpdateClientCommand()
        {
            StringBuilder errorMessage;

            if (!isInputValidated(out errorMessage))
            {
                MessageBox.Show(errorMessage.ToString(), "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Klijent client = new Klijent()
            {
                KlijentId = SelectedClient.KlijentId,
                Ime = FirstName,
                Prezime = LastName,
                BrojTelefona = PhoneNumber,
                EmailAdresa = Email
            };
            UpdateClient?.Invoke(this, new ClientEventArgs(client));

            SetDefaultFieldsAndSelections();
            ShowAllClients();
        }

        private void executeCancelUpdateClientCommand()
        {
            SetDefaultFieldsAndSelections();
        }

        private void executeDeleteClientCommand()
        {
            if (SelectedClient == null)
            {
                MessageBox.Show("Морате изабрати клијента!", "Грешка!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DeleteClient?.Invoke(this, new ClientEventArgs(SelectedClient));
            ShowAllClients();
        }

        private void executeSearchClientsCommand()
        {
            if (string.IsNullOrWhiteSpace(SearchParametar))
            {
                ShowAllClients();
                return;
            }
            ClientsToShow.Clear();
            AllClients.ToList().Where(c => c.Ime.ToUpper().Contains(SearchParametar.Trim().ToUpper()) || c.Prezime.ToUpper().Contains(SearchParametar.Trim().ToUpper())).ToList().ForEach(c => ClientsToShow.Add(c));
        }

        public void SetDefaultFieldsAndSelections()
        {
            CreateMode = true;
            UpdateMode = false;
            SelectedClient = null;
            FirstName = string.Empty;
            LastName = string.Empty;
            PhoneNumber = string.Empty;
            Email = string.Empty;
            SearchParametar = string.Empty;
            ShowAllClients();
        }

        private void ShowAllClients()
        {
            ClientsToShow.Clear();
            AllClients.ToList().ForEach(c => ClientsToShow.Add(c));
        }

        private bool isInputValidated(out StringBuilder errorMessage)
        {
            bool isValidated = true;
            string phoneValidationRegex = @"\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$";

            errorMessage = new StringBuilder("Дошло је до грешке:\r\n");

            if (string.IsNullOrWhiteSpace(FirstName))
            {
                errorMessage.AppendLine("• Морате унети име");
                isValidated = false;
            }
            else if (FirstName.Length < 3 || FirstName.Length > 15)
            {
                errorMessage.AppendLine("• Име мора имати између 3 и 15 карактера");
                isValidated = false;
            }

            if (string.IsNullOrWhiteSpace(LastName))
            {
                errorMessage.AppendLine("• Морате унети презиме");
                isValidated = false;
            }
            else if (LastName.Length < 3 || LastName.Length > 40)
            {
                errorMessage.AppendLine("• Презиме мора имати између 3 и 40 карактера");
                isValidated = false;
            }

            if (string.IsNullOrWhiteSpace(PhoneNumber))
            {
                errorMessage.AppendLine("• Морате унети број телефона");
                isValidated = false;
            }
            else if (!Regex.Match(PhoneNumber, phoneValidationRegex, RegexOptions.IgnoreCase).Success)
            {
                errorMessage.AppendLine("• Унети број телефона није валидан");
                isValidated = false;
            }

            if (string.IsNullOrWhiteSpace(Email))
            {
                errorMessage.AppendLine("• Морате унети е-пошту");
                isValidated = false;
            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(Email);
                }
                catch (FormatException)
                {
                    errorMessage.AppendLine("• Унета е-пошта није валидна");
                    isValidated = false;
                }
            }
            return isValidated;
        }
    }
}
