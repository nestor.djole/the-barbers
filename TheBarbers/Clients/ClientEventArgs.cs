﻿using BusinessEntities;
using System;

namespace TheBarbers.Clients
{
    public class ClientEventArgs : EventArgs
    {
        private readonly Klijent _businessObject;

        public ClientEventArgs(Klijent businessObject)
        {
            _businessObject = businessObject;
        }

        public Klijent BusinessObject
        {
            get { return _businessObject; }
        }        
    }    
}
