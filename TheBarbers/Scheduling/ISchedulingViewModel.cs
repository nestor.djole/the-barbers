﻿using BusinessEntities;
using System;
using System.Collections.ObjectModel;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Scheduling
{
    public interface ISchedulingViewModel: IContainerViewModel
    {
        RelayCommand GoBackCommand { get; }
        RelayCommand CreateEventCommand { get; }
        RelayCommand DeleteEventCommand { get; }
        RelayCommand SearchEventsCommand { get; }

        event EventHandler Started;
        event EventHandler LoadData;
        event EventHandler<SchedulingEventArgs> CreateEvent;
        event EventHandler<SchedulingEventArgs> DeleteEvent;
        event EventHandler Succeeded;

        ObservableCollection<Zakazivanje> AllEvents { get; set; }
        ObservableCollection<Zakazivanje> EventsToShow { get; set; }
        ObservableCollection<Klijent> AllClients { get; set; }
        ObservableCollection<Klijent> ClientsToShow { get; set; }
        ObservableCollection<Zaposleni> Employees { get; set; }
        ObservableCollection<Tretman> Treatments { get; set; }

        void Start();
        void LoadSchedulingData();
    }
}
