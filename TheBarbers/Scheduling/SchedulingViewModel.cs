﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using BusinessEntities;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Scheduling
{
    public class SchedulingViewModel : ContainerViewModelBase, ISchedulingViewModel
    {
        private RelayCommand _goBackCommand;
        private RelayCommand _createEventCommand;
        private RelayCommand _deleteEventCommand;
        private RelayCommand _searchClientsCommand;
        private RelayCommand _searchEventsCommand;

        public ObservableCollection<Zakazivanje> _eventsToShow;

        private Zaposleni _employee;
        private Tretman _treatment;
        private DateTime _date;
        private TimeSpan _time;
        private string _description;
        private string _searchClientsParametar;
        private string _searchEventsParametar;


        public SchedulingViewModel()
        {
            _goBackCommand = new RelayCommand(executeGoBackCommand);
            _createEventCommand = new RelayCommand(executeCreateEventCommand);
            _deleteEventCommand = new RelayCommand(executeDeleteEventCommand);
            _searchClientsCommand = new RelayCommand(executeSearchClientsCommand);
            _searchEventsCommand = new RelayCommand(executeSearchEventsCommand);
            AllEvents = new ObservableCollection<Zakazivanje>();
            EventsToShow = new ObservableCollection<Zakazivanje>();
            AllClients = new ObservableCollection<Klijent>();
            ClientsToShow = new ObservableCollection<Klijent>();
            Employees = new ObservableCollection<Zaposleni>();
            Treatments = new ObservableCollection<Tretman>();
            SetDefaultFieldsAndSelections();
        }

        public Zaposleni SelectedEmployee
        {
            get
            {
                return _employee;
            }
            set
            {
                if (_employee != value)
                {
                    _employee = value;
                    OnPropertyChanged(nameof(this.SelectedEmployee));
                }
            }
        }
        public Tretman SelectedTreatment
        {
            get
            {
                return _treatment;
            }
            set
            {
                if (_treatment != value)
                {
                    _treatment = value;
                    OnPropertyChanged(nameof(this.SelectedTreatment));
                }
            }
        }
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged(nameof(this.Date));
                }
            }
        }
        public TimeSpan Time
        {
            get
            {
                return _time;
            }
            set
            {
                if (_time != value)
                {
                    _time = value;
                    OnPropertyChanged(nameof(this.Time));
                }
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (_description != value)
                {
                    _description = value;
                    OnPropertyChanged(nameof(this.Description));
                }
            }
        }
        public string SearchClientsParametar
        {
            get
            {
                return _searchClientsParametar;
            }
            set
            {
                if (_searchClientsParametar != value)
                {
                    _searchClientsParametar = value;
                    OnPropertyChanged(nameof(this.SearchClientsParametar));
                }
            }
        }
        public string SearchEventsParametar
        {
            get
            {
                return _searchEventsParametar;
            }
            set
            {
                if (_searchEventsParametar != value)
                {
                    _searchEventsParametar = value;
                    OnPropertyChanged(nameof(this.SearchEventsParametar));
                }
            }
        }

        public Klijent SelectedClient { get; set; }
        public Zakazivanje SelectedEvent { get; set; }

        public event EventHandler Started;
        public event EventHandler Succeeded;
        public event EventHandler LoadData;
        public event EventHandler<SchedulingEventArgs> CreateEvent;
        public event EventHandler<SchedulingEventArgs> DeleteEvent;

        public RelayCommand GoBackCommand
        {
            get
            {
                return _goBackCommand;
            }
        }
        public RelayCommand CreateEventCommand
        {
            get
            {
                return _createEventCommand;
            }
        }
        public RelayCommand DeleteEventCommand
        {
            get
            {
                return _deleteEventCommand;
            }
        }
        public RelayCommand SearchClientsCommand
        {
            get
            {
                return _searchClientsCommand;
            }
        }
        public RelayCommand SearchEventsCommand
        {
            get
            {
                return _searchEventsCommand;
            }
        }

        public ObservableCollection<Zakazivanje> AllEvents { get; set; }
        public ObservableCollection<Zakazivanje> EventsToShow
        {
            get
            {
                return _eventsToShow;
            }
            set
            {
                if (_eventsToShow != value)
                {
                    _eventsToShow = value;
                    OnPropertyChanged(nameof(EventsToShow));
                }
            }
        }
        public ObservableCollection<Klijent> AllClients { get; set; }
        public ObservableCollection<Klijent> ClientsToShow { get; set; }
        public ObservableCollection<Zaposleni> Employees { get; set; }
        public ObservableCollection<Tretman> Treatments { get; set; }

        public void Start()
        {
            Started?.Invoke(this, new EventArgs());
            SetDefaultFieldsAndSelections();
        }

        private void executeGoBackCommand()
        {
            Succeeded?.Invoke(this, new EventArgs());
        }

        private void executeCreateEventCommand()
        {
            StringBuilder errorMessage;

            if (!isInputValidated(out errorMessage))
            {
                MessageBox.Show(errorMessage.ToString(), "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            CreateEvent?.Invoke(this, new SchedulingEventArgs(new Zakazivanje() { Datum = Date, Vreme = Time, Klijent = SelectedClient, Zaposleni = SelectedEmployee, Tretman = SelectedTreatment, Opis = Description }));
            ShowAllEvents();
            SetDefaultFieldsAndSelections();
        }

        private void executeDeleteEventCommand()
        {
            if (SelectedEvent == null)
            {
                MessageBox.Show("Морате изабрати догађај!", "Грешка!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DeleteEvent?.Invoke(this, new SchedulingEventArgs(SelectedEvent));
            ShowAllEvents();
            SetDefaultFieldsAndSelections();
        }

        private void executeSearchClientsCommand()
        {
            if (string.IsNullOrWhiteSpace(SearchClientsParametar))
            {
                ShowAllClients();
                return;
            }
            ClientsToShow.Clear();
            AllClients.ToList().Where(c => c.Ime.ToUpper().Contains(SearchClientsParametar.Trim().ToUpper()) || c.Prezime.ToUpper().Contains(SearchClientsParametar.Trim().ToUpper())).ToList().ForEach(c => ClientsToShow.Add(c));
        }

        private void executeSearchEventsCommand()
        {
            if (string.IsNullOrWhiteSpace(SearchEventsParametar))
            {
                ShowAllEvents();
                return;
            }
            EventsToShow.Clear();
            AllEvents.ToList().Where(e => e.Klijent.Ime.ToUpper().Contains(SearchEventsParametar.Trim().ToUpper()) || e.Klijent.Prezime.ToUpper().Contains(SearchEventsParametar.Trim().ToUpper()) || e.Zaposleni.Ime.ToUpper().Contains(SearchEventsParametar.Trim().ToUpper()) || e.Zaposleni.Prezime.ToUpper().Contains(SearchEventsParametar.Trim().ToUpper()) || e.Tretman.NazivTretmana.ToUpper().Contains(SearchEventsParametar.Trim().ToUpper())).ToList().ForEach(c => EventsToShow.Add(c));
        }

        public void LoadSchedulingData()
        {
            LoadData?.Invoke(this, new EventArgs());
            ShowAllClients();
            ShowAllEvents();
            SetDefaultFieldsAndSelections();
        }

        private void ShowAllClients()
        {
            ClientsToShow.Clear();
            AllClients.ToList().ForEach(c => ClientsToShow.Add(c));
        }

        private void ShowAllEvents()
        {
            EventsToShow.Clear();
            AllEvents.ToList().ForEach(c => EventsToShow.Add(c));
        }

        public void SetDefaultFieldsAndSelections()
        {
            SelectedClient = null;
            SelectedEvent = null;
            SelectedEmployee = null;
            SelectedTreatment = null;
            Date = DateTime.Now;
            Time = TimeSpan.Zero;
            Description = string.Empty;
            SearchClientsParametar = string.Empty;
            SearchEventsParametar = string.Empty;
        }

        private bool isInputValidated(out StringBuilder errorMessage)
        {
            bool isValidated = true;

            errorMessage = new StringBuilder("Дошло је до грешке:\r\n");
            if (SelectedClient == null || SelectedEmployee == null || SelectedTreatment == null)
            {
                if (SelectedClient == null)
                    errorMessage.AppendLine("• Морате изабрати клијента");

                if (SelectedEmployee == null)
                    errorMessage.AppendLine("• Морате изабрати запосленог");

                if (SelectedTreatment == null)
                    errorMessage.AppendLine("• Морате изабрати третман");

                isValidated = false;
            }
            else
            {
                if ((GetTotalMinutesFromTimeSpan(Time) < new TimeSpan(8, 0, 0).TotalMinutes) || (GetTotalMinutesFromTimeSpan(Time) + SelectedTreatment.Trajanje) > new TimeSpan(22, 0, 0).TotalMinutes)
                {
                    errorMessage.AppendLine("• Унети термин не одговара радном времену 08:00-22:00");
                    isValidated = false;
                }

                if ((Date.Date < DateTime.Now.Date) || (Date.Date.Equals(DateTime.Now.Date) && Time.Hours < DateTime.Now.Hour) || (Date.Date.Equals(DateTime.Now.Date) && Time.Hours == DateTime.Now.Hour && Time.Minutes < DateTime.Now.Minute))
                {
                    errorMessage.AppendLine("• Унети термин је већ прошао");
                    isValidated = false;
                }

                IEnumerable<Zakazivanje> timeIncompatibleEvents = AllEvents.Where(e => checkTimeCompatibility(e, Date, Time));

                if (timeIncompatibleEvents.Any(e => e.Zaposleni.ZaposleniId.Equals(SelectedEmployee.ZaposleniId)))
                {
                    errorMessage.AppendLine("• Изабрани запослени већ има заказан третман у унетом термину");
                    isValidated = false;
                }
                if (timeIncompatibleEvents.Any(e => e.Klijent.KlijentId.Equals(SelectedClient.KlijentId)))
                {
                    errorMessage.AppendLine("• Изабрани клијент већ има заказан третман у унетом термину");
                    isValidated = false;
                }
            }

            return isValidated;
        }

        private bool checkTimeCompatibility(Zakazivanje e, DateTime date, TimeSpan time)
        {
            return Date.Date.Equals(e.Datum.Date) && GetTotalMinutesFromTimeSpan(e.Vreme) < GetTotalMinutesFromTimeSpan(Time) && GetTotalMinutesFromTimeSpan(Time) < GetTotalMinutesFromTimeSpan(e.Vreme) + e.Tretman.Trajanje;
        }

        private int GetTotalMinutesFromTimeSpan(TimeSpan time)
        {
            return time.Hours * 60 + time.Minutes;
        }

    }
}
