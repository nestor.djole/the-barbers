﻿using BusinessEntities;
using System;

namespace TheBarbers.Scheduling
{
    public class SchedulingEventArgs : EventArgs
    {
        private readonly Zakazivanje _businessObject;

        public SchedulingEventArgs(Zakazivanje businessObject)
        {
            _businessObject = businessObject;
        }

        public Zakazivanje BusinessObject
        {
            get { return _businessObject; }
        }
    }
}
