﻿using System;
using TheBarbers.Base.ViewModel;
using TheBarbers.Clients;
using TheBarbers.Home;
using TheBarbers.Scheduling;
using TheBarbers.Treatments;
using BusinessEntities.TransferModels;
using System.Collections.Generic;
using BusinessEntities;
using System.Windows;
using TheBarbers.Services;
using System.Linq;

namespace TheBarbers.Main
{
    public class MainWindowViewModel : ContainerViewModelBase, IMainPageViewModel
    {
        private IHomeViewModel _homeViewModel;
        private IClientsViewModel _clientsViewModel;
        private ITreatmentsViewModel _treatmentsViewModel;
        private ISchedulingViewModel _schedulingViewModel;

        public MainWindowViewModel(
            IHomeViewModel homeViewModel,
            IClientsViewModel clientsViewModel,
            ITreatmentsViewModel treatmentsViewModel,
            ISchedulingViewModel schedulingViewModel
            )
        {
            _homeViewModel = homeViewModel;
            _homeViewModel.Started += homeViewModel_Started;
            _homeViewModel.Succeeded += homeViewModel_Succeeded;

            _clientsViewModel = clientsViewModel;
            _clientsViewModel.Started += clientsViewModel_Started;
            _clientsViewModel.LoadData += clientsViewModel_LoadData;
            _clientsViewModel.CreateClient += clientsViewModel_CreateClient;
            _clientsViewModel.UpdateClient += clientsViewModel_UpdateClient;
            _clientsViewModel.DeleteClient += clientsViewModel_DeleteClient;
            _clientsViewModel.Succeeded += clientsViewModel_Succeeded;

            _treatmentsViewModel = treatmentsViewModel;
            _treatmentsViewModel.Started += treatmentsViewModel_Started;
            _treatmentsViewModel.LoadData += treatmentsViewModel_LoadData;
            _treatmentsViewModel.CreateTreatment += treatmentsViewModel_CreateTreatment;
            _treatmentsViewModel.UpdateTreatment += treatmentsViewModel_UpdateTreatment;
            _treatmentsViewModel.DeleteTreatment += treatmentsViewModel_DeleteTreatment;
            _treatmentsViewModel.Succeeded += treatmentsViewModel_Succeeded;

            _schedulingViewModel = schedulingViewModel;
            _schedulingViewModel.Started += schedulingViewModel_Started;
            _schedulingViewModel.LoadData += schedulingViewModel_LoadData;
            _schedulingViewModel.CreateEvent += schedulingViewModel_CreateEvent;
            _schedulingViewModel.DeleteEvent += schedulingViewModel_DeleteEvent;
            _schedulingViewModel.Succeeded += schedulingViewModel_Succeeded;

            setHomePageCurrent();
        }

        private void setHomePageCurrent()
        {
            _homeViewModel.Start();
        }

        private void homeViewModel_Started(object sender, EventArgs e)
        {
            CurrentViewModel = _homeViewModel;
        }

        private void homeViewModel_Succeeded(object sender, EventArgs e)
        {
            try
            {
                if(CommunicationService.Instance.IsConnectionOpen())
                {
                    switch (_homeViewModel.Result.Value)
                    {
                        case HomeViewModelResultType.RequestClients:
                            CurrentViewModel = null;
                            _clientsViewModel.Start();
                            _clientsViewModel.LoadClientsData();
                            break;
                        case HomeViewModelResultType.RequestTreatments:
                            CurrentViewModel = null;
                            _treatmentsViewModel.Start();
                            _treatmentsViewModel.LoadTreatmentsData();
                            break;
                        case HomeViewModelResultType.RequestScheduling:
                            CurrentViewModel = null;
                            _schedulingViewModel.Start();
                            _schedulingViewModel.LoadSchedulingData();
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
                else
                {
                    MessageBox.Show("Дошло је до грешке приликом конекције на сервер!", "Сервер недоступан!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Дошло је до грешке приликом конекције на сервер!", "Сервер недоступан!", MessageBoxButton.OK, MessageBoxImage.Error);
            }           
        }

        private void clientsViewModel_Started(object sender, EventArgs e)
        {
            CurrentViewModel = _clientsViewModel;
        }

        private void clientsViewModel_LoadData(object sender, EventArgs e)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.GET_ALL_CLIENTS, new Klijent());
                _clientsViewModel.AllClients.Clear();
                ((List<Klijent>)result).OrderBy(c => c.Prezime).ToList().ForEach(c => _clientsViewModel.AllClients.Add(c));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void clientsViewModel_CreateClient(object sender, ClientEventArgs cea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.INSERT_CLIENT, cea.BusinessObject);
                Klijent newClient = (Klijent)result;
                _clientsViewModel.AllClients.Add(newClient);
                MessageBox.Show($"Успешно сте унели клијента:\r\nИме: {newClient.Ime}\r\nПрезиме: {newClient.Prezime}\r\nБрој телефона: {newClient.BrojTelefona}\r\nЕ-пошта: {newClient.EmailAdresa}", "Успешно креирање клијента!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void clientsViewModel_UpdateClient(object sender, ClientEventArgs cea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.UPDATE_CLIENT, cea.BusinessObject);
                Klijent updatedClient = (Klijent)result;
                Klijent oldClientData = _clientsViewModel.AllClients.First(c => c.KlijentId == updatedClient.KlijentId);
                _clientsViewModel.AllClients.Remove(oldClientData);
                _clientsViewModel.AllClients.Add(updatedClient);
                MessageBox.Show($"Успешно сте изменили клијента:\r\nИме: {updatedClient.Ime}\r\nПрезиме: {updatedClient.Prezime}\r\nБрој телефона: {updatedClient.BrojTelefona}\r\nЕ-пошта: {updatedClient.EmailAdresa}", "Успешно ажурирање клијента!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void clientsViewModel_DeleteClient(object sender, ClientEventArgs cea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.DELETE_CLIENT, cea.BusinessObject);
                Klijent deletedClient = _clientsViewModel.AllClients.First(c => c.KlijentId == ((Klijent)result).KlijentId);
                _clientsViewModel.AllClients.Remove(deletedClient);
                MessageBox.Show($"Успешно сте избрисали клијента:\r\nИме: {deletedClient.Ime}\r\nПрезиме: {deletedClient.Prezime}\r\nБрој телефона: {deletedClient.BrojTelefona}\r\nЕ-пошта: {deletedClient.EmailAdresa}", "Успешно брисање клијента!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void clientsViewModel_Succeeded(object sender, EventArgs e)
        {
            CurrentViewModel = null;
            setHomePageCurrent();
        }

        private void treatmentsViewModel_Started(object sender, EventArgs e)
        {
            CurrentViewModel = _treatmentsViewModel;
        }

        private void treatmentsViewModel_LoadData(object sender, EventArgs e)
        {
            try
            {
                object treatments = CommunicationService.Instance.SendRequest(Operation.GET_ALL_TREATMENTS, new Tretman());
                object treatmentTypes = CommunicationService.Instance.SendRequest(Operation.GET_ALL_TREATMENTTYPES, new VrstaTretmana());

                _treatmentsViewModel.AllTreatments.Clear();
                _treatmentsViewModel.TreatmentTypes.Clear();

                ((List<Tretman>)treatments).OrderBy(t => t.NazivTretmana).ToList().ForEach(t => _treatmentsViewModel.AllTreatments.Add(t));
                ((List<VrstaTretmana>)treatmentTypes).OrderBy(tt => tt.NazivVrsteTretmana).ToList().ForEach(tt => _treatmentsViewModel.TreatmentTypes.Add(tt));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void treatmentsViewModel_CreateTreatment(object sender, TreatmentEventArgs tea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.INSERT_TREATMENT, tea.BusinessObject);
                Tretman newTreatment = (Tretman)result;
                _treatmentsViewModel.AllTreatments.Add(newTreatment);
                MessageBox.Show($"Успешно сте унели третман:\r\nНазив: {newTreatment.NazivTretmana}\r\nТрајање: {newTreatment.Trajanje} мин\r\nВрста третмана: {newTreatment.Vrsta}", "Успешно креирање третмана!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void treatmentsViewModel_UpdateTreatment(object sender, TreatmentEventArgs tea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.UPDATE_TREATMENT, tea.BusinessObject);
                Tretman updatedTreatment= (Tretman)result;
                Tretman oldTreatmentData = _treatmentsViewModel.AllTreatments.First(t => t.TretmanId== updatedTreatment.TretmanId);
                _treatmentsViewModel.AllTreatments.Remove(oldTreatmentData);
                _treatmentsViewModel.AllTreatments.Add(updatedTreatment);
                MessageBox.Show($"Успешно сте изменили третман:\r\nНазив: {updatedTreatment.NazivTretmana}\r\nТрајање: {updatedTreatment.Trajanje} мин\r\nВрста третмана: {updatedTreatment.Vrsta}", "Успешно ажурирање третмана!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void treatmentsViewModel_DeleteTreatment(object sender, TreatmentEventArgs tea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.DELETE_TREATMENT, tea.BusinessObject);
                Tretman deletedTreatment = _treatmentsViewModel.AllTreatments.First(t => t.TretmanId== ((Tretman)result).TretmanId);
                _treatmentsViewModel.AllTreatments.Remove(deletedTreatment);
                MessageBox.Show($"Успешно сте избрисали третман:\r\nНазив: {deletedTreatment.NazivTretmana}\r\nТрајање: {deletedTreatment.Trajanje} мин\r\nВрста третмана: {deletedTreatment.Vrsta}", "Успешно брисање третмана!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void treatmentsViewModel_Succeeded(object sender, EventArgs e)
        {
            CurrentViewModel = null;
            setHomePageCurrent();
        }

        private void schedulingViewModel_Started(object sender, EventArgs e)
        {
            CurrentViewModel = _schedulingViewModel;
        }

        private void schedulingViewModel_LoadData(object sender, EventArgs e)
        {
            try
            {
                object clients = CommunicationService.Instance.SendRequest(Operation.GET_ALL_CLIENTS, new Klijent());
                object treatments = CommunicationService.Instance.SendRequest(Operation.GET_ALL_TREATMENTS, new Tretman());
                object events = CommunicationService.Instance.SendRequest(Operation.GET_ALL_EVENTS, new Zakazivanje());
                object employees = CommunicationService.Instance.SendRequest(Operation.GET_ALL_EMPLOYEES, new Zaposleni());

                _schedulingViewModel.AllEvents.Clear();
                _schedulingViewModel.AllClients.Clear();
                _schedulingViewModel.Treatments.Clear();
                _schedulingViewModel.Employees.Clear();

                ((List<Klijent>)clients).OrderBy(c => c.Prezime).ToList().ForEach(c => _schedulingViewModel.AllClients.Add(c));
                ((List<Zakazivanje>)events).OrderBy(ev => ev.Datum).ToList().ForEach(ev => _schedulingViewModel.AllEvents.Add(ev));
                ((List<Tretman>)treatments).OrderBy(t => t.NazivTretmana).ToList().ForEach(t => _schedulingViewModel.Treatments.Add(t));
                ((List<Zaposleni>)employees).OrderBy(em => em.Prezime).ToList().ForEach(em => _schedulingViewModel.Employees.Add(em));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void schedulingViewModel_CreateEvent(object sender, SchedulingEventArgs sea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.INSERT_EVENT, sea.BusinessObject);
                Zakazivanje newEvent = (Zakazivanje)result;
                _schedulingViewModel.AllEvents.Add(newEvent);
                MessageBox.Show($"Успешно сте заказали догађај:\r\nДатум: {newEvent.Datum.ToShortDateString()}\r\nВреме: {newEvent.Vreme.ToString("hh\\:mm")}\r\nТретман: {newEvent.Tretman}\r\nКлијент: {newEvent.Klijent}\r\nЗапослени: {newEvent.Zaposleni}", "Успешно заказивање!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }        

        private void schedulingViewModel_DeleteEvent(object sender, SchedulingEventArgs sea)
        {
            try
            {
                object result = CommunicationService.Instance.SendRequest(Operation.DELETE_EVENT, sea.BusinessObject);
                Zakazivanje deletedEvent = _schedulingViewModel.AllEvents.First(t =>  t.Equals((Zakazivanje)result));
                _schedulingViewModel.AllEvents.Remove(deletedEvent);
                MessageBox.Show($"Успешно сте избрисали заказани догађај:\r\nДатум: {deletedEvent.Datum.ToShortDateString()}\r\nВреме: {deletedEvent.Vreme.ToString("hh\\:mm")}\r\nТретман: {deletedEvent.Tretman}\r\nКлијент: {deletedEvent.Klijent}\r\nЗапослени: {deletedEvent.Zaposleni}", "Успешно отказивање!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void schedulingViewModel_Succeeded(object sender, EventArgs e)
        {
            CurrentViewModel = null;
            setHomePageCurrent();
        }
    }
}