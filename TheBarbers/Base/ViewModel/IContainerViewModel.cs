﻿namespace TheBarbers.Base.ViewModel
{
    public interface IContainerViewModel : IViewModel
    {
        IViewModel CurrentViewModel { get; }
    }
}
