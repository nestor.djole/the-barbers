﻿using BusinessEntities;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Treatments
{
    public class TreatmentsViewModel : ContainerViewModelBase, ITreatmentsViewModel
    {
        private RelayCommand _goBackCommand;
        private RelayCommand _createTreatmentCommand;
        private RelayCommand _startUpdateTreatmentCommand;
        private RelayCommand _updateTreatmentCommand;
        private RelayCommand _cancelUpdateTreatmentCommand;
        private RelayCommand _deleteTreatmentCommand;
        private RelayCommand _searchTreatmentsCommand;

        public ObservableCollection<Tretman> _treatmentsToShow;

        private string _name;
        private int _duration;
        private VrstaTretmana _type;
        private string _searchParametar;

        private bool _createMode;
        private bool _updateMode;

        public TreatmentsViewModel()
        {
            _goBackCommand = new RelayCommand(executeGoBackCommand);
            _createTreatmentCommand = new RelayCommand(executeCreateTreatmentCommand);
            _startUpdateTreatmentCommand = new RelayCommand(executeStartUpdateTreatmentCommand);
            _updateTreatmentCommand = new RelayCommand(executeUpdateTreatmentCommand);
            _cancelUpdateTreatmentCommand = new RelayCommand(executeCancelUpdateTreatmentCommand);
            _deleteTreatmentCommand = new RelayCommand(executeDeleteTreatmentCommand);
            _searchTreatmentsCommand = new RelayCommand(executeSearchTreatmentsCommand);
            AllTreatments = new ObservableCollection<Tretman>();
            TreatmentsToShow = new ObservableCollection<Tretman>();
            TreatmentTypes = new ObservableCollection<VrstaTretmana>();
            SetDefaultFieldsAndSelections();
        }
        
        public event EventHandler Started;
        public event EventHandler LoadData;
        public event EventHandler<TreatmentEventArgs> CreateTreatment;
        public event EventHandler<TreatmentEventArgs> UpdateTreatment;
        public event EventHandler<TreatmentEventArgs> DeleteTreatment;
        public event EventHandler Succeeded;

        public RelayCommand GoBackCommand
        {
            get
            {
                return _goBackCommand;
            }
        }
        public RelayCommand CreateTreatmentCommand
        {
            get
            {
                return _createTreatmentCommand;
            }
        }
        public RelayCommand StartUpdateTreatmentCommand
        {
            get
            {
                return _startUpdateTreatmentCommand;
            }
        }
        public RelayCommand UpdateTreatmentCommand
        {
            get
            {
                return _updateTreatmentCommand;
            }
        }
        public RelayCommand CancelUpdateTreatmentCommand
        {
            get
            {
                return _cancelUpdateTreatmentCommand;
            }
        }
        public RelayCommand DeleteTreatmentCommand
        {
            get
            {
                return _deleteTreatmentCommand;
            }
        }
        public RelayCommand SearchTreatmentsCommand
        {
            get
            {
                return _searchTreatmentsCommand;
            }
        }

        public ObservableCollection<Tretman> AllTreatments { get; set; }
        public ObservableCollection<Tretman> TreatmentsToShow
        {
            get
            {
                return _treatmentsToShow;
            }
            set
            {
                if (_treatmentsToShow != value)
                {
                    _treatmentsToShow = value;
                    OnPropertyChanged(nameof(this.TreatmentsToShow));
                }
            }
        }        
        public ObservableCollection<VrstaTretmana> TreatmentTypes { get; set; }

        public Tretman SelectedTreatment { get; set; }       

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged(nameof(this.Name));
                }
            }
        }
        public int Duration
        {
            get
            {
                return _duration;
            }
            set
            {
                if (_duration != value)
                {
                    _duration = value;
                    OnPropertyChanged(nameof(this.Duration));
                }
            }
        }
        public VrstaTretmana SelectedTreatmentType
        {
            get
            {
                return _type;
            }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged(nameof(this.SelectedTreatmentType));
                }
            }
        }
        public string SearchParametar
        {
            get
            {
                return _searchParametar;
            }
            set
            {
                if (_searchParametar != value)
                {
                    _searchParametar = value;
                    OnPropertyChanged(nameof(this.SearchParametar));
                }
            }
        }

        public bool CreateMode
        {
            get
            {
                return _createMode;
            }
            set
            {
                if (_createMode != value)
                {
                    _createMode = value;
                    OnPropertyChanged(nameof(this.CreateMode));
                }
            }
        }
        public bool UpdateMode
        {
            get
            {
                return _updateMode;
            }
            set
            {
                if (_updateMode != value)
                {
                    _updateMode = value;
                    OnPropertyChanged(nameof(this.UpdateMode));
                }
            }
        }

        public void Start()
        {
            Started?.Invoke(this, new EventArgs());
            SetDefaultFieldsAndSelections();
        }

        public void LoadTreatmentsData()
        {
            LoadData?.Invoke(this, new EventArgs());
            ShowAllTreatments();
        }

        private void executeGoBackCommand()
        {
            Succeeded?.Invoke(this, new EventArgs());
        }

        private void executeCreateTreatmentCommand()
        {
            StringBuilder errorMessage;

            if (!isInputValidated(out errorMessage))
            {
                MessageBox.Show(errorMessage.ToString(), "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            CreateTreatment?.Invoke(this, new TreatmentEventArgs(new Tretman() { NazivTretmana = Name, Trajanje = Duration, Vrsta = SelectedTreatmentType}));
            SetDefaultFieldsAndSelections();
            ShowAllTreatments();
        }
        
        private void executeStartUpdateTreatmentCommand()
        {
            if (SelectedTreatment == null)
            {
                MessageBox.Show("Морате изабрати третман!", "Грешка!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            CreateMode = false;
            UpdateMode = true;

            Name = SelectedTreatment.NazivTretmana;
            Duration = SelectedTreatment.Trajanje;
            SelectedTreatmentType = SelectedTreatment.Vrsta;
        }

        private void executeUpdateTreatmentCommand()
        {
            StringBuilder errorMessage;

            if (!isInputValidated(out errorMessage))
            {
                MessageBox.Show(errorMessage.ToString(), "Грешка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Tretman treatment = new Tretman()
            {
                TretmanId = SelectedTreatment.TretmanId,
                NazivTretmana = Name,
                Trajanje = Duration,
                Vrsta = SelectedTreatmentType
            };
            UpdateTreatment?.Invoke(this, new TreatmentEventArgs(treatment));

            SetDefaultFieldsAndSelections();
        }

        private void executeCancelUpdateTreatmentCommand()
        {
            SetDefaultFieldsAndSelections();
        }

        private void executeDeleteTreatmentCommand()
        {
            if (SelectedTreatment == null)
            {
                MessageBox.Show("Морате изабрати третман!", "Грешка!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DeleteTreatment?.Invoke(this, new TreatmentEventArgs(SelectedTreatment));
            SetDefaultFieldsAndSelections();
        }

        private void executeSearchTreatmentsCommand()
        {
            if (string.IsNullOrWhiteSpace(SearchParametar))
            {
                ShowAllTreatments();
                return;
            }
            TreatmentsToShow.Clear();
            AllTreatments.ToList().Where(t => t.NazivTretmana.ToUpper().Contains(SearchParametar.Trim().ToUpper())).ToList().ForEach(t => TreatmentsToShow.Add(t));
        }

        public void SetDefaultFieldsAndSelections()
        {
            CreateMode = true;
            UpdateMode = false;
            SelectedTreatment = null;
            Name = string.Empty;
            Duration = 0;
            SelectedTreatmentType = null;
            SearchParametar = string.Empty;
        }

        private void ShowAllTreatments()
        {
            TreatmentsToShow.Clear();
            AllTreatments.ToList().ForEach(t => TreatmentsToShow.Add(t));
        }

        private bool isInputValidated(out StringBuilder errorMessage)
        {
            bool isValidated = true;

            errorMessage = new StringBuilder("Дошло је до грешке:\r\n");

            if (string.IsNullOrWhiteSpace(Name))
            {
                errorMessage.AppendLine("• Морате унети назив третмана");
                isValidated = false;
            }
            else if (Name.Length < 6 || Name.Length > 40)
            {
                errorMessage.AppendLine("• Назив третмана мора имати између 6 и 40 карактера");
                isValidated = false;
            }

            if (Duration < 15 || Duration > 120)
            {
                errorMessage.AppendLine("• Третман може да траје најмање 15, а највише 120 минута");
                isValidated = false;
            }

            if (SelectedTreatmentType == null)
            {
                errorMessage.AppendLine("• Морате изабрати врсту третмана");
                isValidated = false;
            }

            return isValidated;
        }
    }
}
