﻿using BusinessEntities;
using System;
using System.Collections.ObjectModel;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Treatments
{
    public interface ITreatmentsViewModel : IContainerViewModel
    {
        RelayCommand GoBackCommand { get; }
        RelayCommand CreateTreatmentCommand { get; }
        RelayCommand UpdateTreatmentCommand { get; }
        RelayCommand DeleteTreatmentCommand { get; }
        RelayCommand SearchTreatmentsCommand { get; }

        event EventHandler Started;
        event EventHandler LoadData;
        event EventHandler<TreatmentEventArgs> CreateTreatment;
        event EventHandler<TreatmentEventArgs> UpdateTreatment;
        event EventHandler<TreatmentEventArgs> DeleteTreatment;
        event EventHandler Succeeded;
        
        ObservableCollection<Tretman> AllTreatments { get; set; }
        ObservableCollection<Tretman> TreatmentsToShow { get; set; }
        ObservableCollection<VrstaTretmana> TreatmentTypes { get; set; }

        void Start();
        void LoadTreatmentsData();
    }
}
