﻿using BusinessEntities;
using System;

namespace TheBarbers.Treatments
{
    public class TreatmentEventArgs : EventArgs
    {
        private readonly Tretman _businessObject;

        public TreatmentEventArgs(Tretman businessObject)
        {
            _businessObject = businessObject;
        }

        public Tretman BusinessObject
        {
            get { return _businessObject; }
        }
    }
}
