﻿using System;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Home
{
    public interface IHomeViewModel : IViewModel
    {
        RelayCommand ClientsCommand { get; }
        RelayCommand TreatmentsCommand { get; }
        RelayCommand SchedulingCommand { get; }

        event EventHandler Started;
        event EventHandler Succeeded;

        HomeViewModelResultType? Result { get; }

        void Start();
    }
}
