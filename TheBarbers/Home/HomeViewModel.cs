﻿using System;
using TheBarbers.Base.ViewModel;
using TheBarbers.Common;

namespace TheBarbers.Home
{
    public enum HomeViewModelResultType
    {
        RequestClients,
        RequestTreatments,
        RequestScheduling,
    }

    public class HomeViewModel : ViewModelBase, IHomeViewModel
    {
        private RelayCommand _clientsCommand;
        private RelayCommand _treatmentsCommand;
        private RelayCommand _schedulingCommand;

        public event EventHandler Started;
        public event EventHandler Succeeded;

        private HomeViewModelResultType? _result;      

        public HomeViewModel()
        {
            _clientsCommand = new RelayCommand(executeClientsCommand);
            _treatmentsCommand = new RelayCommand(executeTreatmentsCommand);
            _schedulingCommand = new RelayCommand(executeSchedulingCommand);
        }

        public void Start()
        {
            Started?.Invoke(this, new EventArgs());
        }

        public RelayCommand ClientsCommand
        {
            get
            {
                return _clientsCommand;
            }
        }

        public RelayCommand TreatmentsCommand
        {
            get
            {
                return _treatmentsCommand;
            }
        }

        public RelayCommand SchedulingCommand
        {
            get
            {
                return _schedulingCommand;
            }
        }

        public HomeViewModelResultType? Result { get => _result; }

        private void executeClientsCommand()
        {
            _result = HomeViewModelResultType.RequestClients;
            Succeeded?.Invoke(this, new EventArgs());
        }

        private void executeTreatmentsCommand()
        {
            _result = HomeViewModelResultType.RequestTreatments;
            Succeeded?.Invoke(this, new EventArgs());
        }

        private void executeSchedulingCommand()
        {
            _result = HomeViewModelResultType.RequestScheduling;
            Succeeded?.Invoke(this, new EventArgs());
        }
    }
}