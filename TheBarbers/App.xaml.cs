﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;
using TheBarbers.Clients;
using TheBarbers.Home;
using TheBarbers.Main;
using TheBarbers.Scheduling;
using TheBarbers.Treatments;

namespace TheBarbers
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IServiceProvider ServiceProvider { get; private set; }
        public IConfiguration ConfigurationProvider { get; private set; }

        public App()
        {
            ServiceProvider = createServiceProvider(ConfigurationProvider);
        }

        private IServiceProvider createServiceProvider(IConfiguration configuration)
        {
            IServiceCollection serviceCollection = new ServiceCollection();

            serviceCollection.AddTransient<IMainPageViewModel, MainWindowViewModel>();
            serviceCollection.AddTransient<IHomeViewModel, HomeViewModel>();
            serviceCollection.AddTransient<IClientsViewModel, ClientsViewModel>();
            serviceCollection.AddTransient<ITreatmentsViewModel, TreatmentsViewModel>();
            serviceCollection.AddTransient<ISchedulingViewModel, SchedulingViewModel>();

            return serviceCollection.BuildServiceProvider();
        }

    }
}
