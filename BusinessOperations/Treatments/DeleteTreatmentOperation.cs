﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Treatments
{
    public class DeleteTreatmentOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            return DatabaseBroker.Broker.Delete((Tretman)obj);
        }
    }
}
