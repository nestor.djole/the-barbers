﻿using BusinessEntities;
using DataAccessComponents;
using System.Collections.Generic;
using System.Linq;

namespace BusinessOperations.Treatments
{
    public class GetAllTreatmentsOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            List<AbstractBusinessEntity> treatmentsBusinessEntities = DatabaseBroker.Broker.Select((Tretman)obj);
            List<Tretman> treatments = treatmentsBusinessEntities.Cast<Tretman>().ToList();
            treatments.ForEach(t => t.Vrsta = (VrstaTretmana)DatabaseBroker.Broker.GetById(t.Vrsta));
            return treatments;
        }
    }
}
