﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Treatments
{
    public class UpdateTreatmentOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Tretman t = (Tretman)obj;
            return DatabaseBroker.Broker.Update(t);
        }
    }
}
