﻿using BusinessEntities;
using DataAccessComponents;
using System.Collections.Generic;
using System.Linq;

namespace BusinessOperations.Treatments
{
    public class GetAllTreatmentTypesOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            List<AbstractBusinessEntity> treatmentsTypesBusinessEntities = DatabaseBroker.Broker.Select((VrstaTretmana)obj);
            List<VrstaTretmana> treatmentTypes = treatmentsTypesBusinessEntities.Cast<VrstaTretmana>().ToList();
            return treatmentTypes;
        }
    }
}
