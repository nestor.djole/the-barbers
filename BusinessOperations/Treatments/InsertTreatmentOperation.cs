﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Treatments
{
    public class InsertTreatmentOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Tretman t = (Tretman)obj;
            t.TretmanId = DatabaseBroker.Broker.GetNewID(t);
            return DatabaseBroker.Broker.Create(t);
        }
    }
}
