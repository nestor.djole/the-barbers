﻿using BusinessEntities;
using DataAccessComponents;
using System.Collections.Generic;
using System.Linq;

namespace BusinessOperations.Employees
{
    public class GetAllEmployeesOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            List<AbstractBusinessEntity> employeesBusinessEntities = DatabaseBroker.Broker.Select((Zaposleni)obj);
            List<Zaposleni> employees = employeesBusinessEntities.Cast<Zaposleni>().ToList();
            return employees;
        }
    }
}
