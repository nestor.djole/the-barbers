﻿using DataAccessComponents;
using System;

namespace BusinessOperations
{
    public abstract class BaseBusinessOperation
    {
        private object _result;

        public object ExecuteOperation(object _object)
        {
            DatabaseBroker broker = null;
            try
            {
                broker = DatabaseBroker.Broker;
                broker.OpenConnection();
                broker.BeginTransaction();
                _result = Execute(_object);
                broker.CommitTransaction();
                return _result;
            }
            catch (Exception ex)
            {
                if (broker != null)
                    broker.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                broker.CloseConnection();
            }
        }

        protected abstract object Execute(object obj);
    }
}
