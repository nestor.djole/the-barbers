﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Scheduling
{
    public class InsertEventOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Zakazivanje e = (Zakazivanje)obj;
            return DatabaseBroker.Broker.Create(e);
        }
    }
}
