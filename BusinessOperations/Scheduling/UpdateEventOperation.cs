﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Scheduling
{
    public class UpdateEventOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Zakazivanje e = (Zakazivanje)obj;
            return DatabaseBroker.Broker.Update(e);
        }
    }
}
