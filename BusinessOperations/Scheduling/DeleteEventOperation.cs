﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Scheduling
{
    public class DeleteEventOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            return DatabaseBroker.Broker.Delete((Zakazivanje)obj);
        }
    }
}
