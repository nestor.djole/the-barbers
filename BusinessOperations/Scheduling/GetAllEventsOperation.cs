﻿using BusinessEntities;
using DataAccessComponents;
using System.Collections.Generic;
using System.Linq;

namespace BusinessOperations.Scheduling
{
    public class GetAllEventsOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            List<AbstractBusinessEntity> eventsBusinessEntities = DatabaseBroker.Broker.Select((Zakazivanje)obj);
            List<Zakazivanje> events = eventsBusinessEntities.Cast<Zakazivanje>().ToList();
            foreach(Zakazivanje ev in events)
            {
                ev.Tretman = (Tretman)DatabaseBroker.Broker.GetById(ev.Tretman);
                ev.Zaposleni = (Zaposleni)DatabaseBroker.Broker.GetById(ev.Zaposleni);
                ev.Klijent = (Klijent)DatabaseBroker.Broker.GetById(ev.Klijent);
            }

            return events;            
        }
    }
}
