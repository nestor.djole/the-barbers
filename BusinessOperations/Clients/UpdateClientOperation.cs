﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Clients
{
    public class UpdateClientOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Klijent c = (Klijent)obj;
            return DatabaseBroker.Broker.Update(c);
        }
    }
}
