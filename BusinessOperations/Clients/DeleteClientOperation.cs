﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Clients
{
    public class DeleteClientOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            return DatabaseBroker.Broker.Delete((Klijent)obj);
        }
    }
}
