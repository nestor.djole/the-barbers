﻿using BusinessEntities;
using DataAccessComponents;
using System.Collections.Generic;
using System.Linq;

namespace BusinessOperations.Clients
{
    public class GetAllClientsOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            List<AbstractBusinessEntity> clientsBusinessEntities = DatabaseBroker.Broker.Select((Klijent)obj);
            List<Klijent> clients = clientsBusinessEntities.Cast<Klijent>().ToList();
            return clients;
        }
    }
}
