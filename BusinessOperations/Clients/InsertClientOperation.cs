﻿using BusinessEntities;
using DataAccessComponents;

namespace BusinessOperations.Clients
{
    public class InsertClientOperation : BaseBusinessOperation
    {
        protected override object Execute(object obj)
        {
            Klijent c = (Klijent)obj;
            c.KlijentId = DatabaseBroker.Broker.GetNewID(c);
            return DatabaseBroker.Broker.Create(c);            
        }
    }
}
